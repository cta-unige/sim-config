# CONFIGURATION NAME
LST_SIPM
# ARRAY LAYOUT
2 MAGIC
4 LST
1 MST
# MAGIC CONFIGURATION
# LST CONFIGURATION
SiPM-based LST:
- 7420 pixels
- LCT2 SiPM
- Pulse shape: 7ns template
- SPE template: 12ns
# MST CONFIGURATION
#TRIGGER CONFIGURATION
- Similar to current LST
- 3 trigger towers with 28 pixels each (84 total)
