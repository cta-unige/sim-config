% Configuration file for a LST in Prod-4 (CTA-PROD4 setup) with SiPMs instead of PMTs


  echo ************************************************************************
#ifdef TELESCOPE
  echo Configuration for large-size telescope $(TELESCOPE) in Prod-4.
#else
  echo Global configuration parameters for large-size telescopes in Prod-4.
#endif
  echo ************************************************************************
  echo

% Start with configuration items not specific for LST.

#include <CTA-PROD4-site.cfg>
#include <CTA-PROD4-common.cfg>

% The remainder is specific for LST with small SiPM pixels
% specific configuration values with the tag Config1 at the end, overriding former values

% ------------------------- Optical parameters --------------------------

optics_config_name = LST
optics_config_version = 2018-09-20

parabolic_dish      = 1        % Mirrors are on a parabolic dish.
focal_length        = 2800     % Nominal focal length [cm]. Effective focal length is 2930.6 cm based on optical ray-tracing.
effective_focal_length = 2930.565 % Only to be used for image analysis. No effect in simulation itself.
mirror_focal_length = 0        % Adapted automatically unless specified in file (idea is to have three groups or sort)
dish_shape_length   = 2800     % Same as nominal focal length for strictly parabolic design

#ifdef PERFECT_DISH
% If all mirrors are just perfect:
mirror_list         = mirror_CTA-LST-v20141201-198.dat   % 386.9 m^2 projected
random_focal_length            = 0.

mirror_reflection_random_angle = 0.0
mirror_align_random_distance   = 0.0
mirror_align_random_horizontal = 0.0,0.,0.,0.
mirror_align_random_vertical   = 0.0,0.,0.,0.
#else
% Better try to use a realistic configuration:
mirror_list         = mirror_CTA-LST-flen_grouped.dat   % Three groups of focal lengths (negative flen values! random flen still applies)
random_focal_length =  0,20    % Assume flat distribution with a maximum of +-20 cm deviation from the given focal length
mirror_reflection_random_angle = 0.0039 % [deg]
mirror_align_random_distance   = 0.0 % cm
% Preliminary values, to be revised:
mirror_align_random_horizontal = 0.0025,28.,0.0,0.0 % no zenith dependence due to active mirror control
mirror_align_random_vertical   = 0.0025,28.,0.0,0.0 % no zenith dependence due to active mirror control
#endif

mirror_offset       = 0.       % 0.: Axes crossing at dish center.
focus_offset        = 6.55     % 1./(1./2800.-1./12.e5)-2800. (focusing at 12 km distance)

mirror_reflectivity = ref_200_1100_180314_ver2.1a.dat % Koji Noda, 2018-03-15

% Accuracy of the tracking and measurement of current direction.
% Pointing errors can always be mimicked at the analysis level:
telescope_random_angle         = 0.
telescope_random_error         = 0.

% -------------------------- Camera ------------------------------

camera_config_name = LST
camera_config_version = 2019-09-19

camera_body_shape = 2        % Square camera body, needs newer sim_telarray
camera_body_diameter = 300   % cm (only for shadowing, 3 m * 3 m square)

% The telescope_transmission factor corrects for the shadowing by
% elements not explicitly included in sim_telarray.
% It is thus the ratio of the effective optical area (in projection) based on a
% detailed ray-tracing with all elements over the same area in simplified
% ray-tracing by sim_telarray.
% The original evaluation by detailed ray-tracing (K.Noda, 2013, with ROBAST)
% resulted in an un-projected non-shadowed mirror area of 369.14+-0.08 m^2, from
% which after projection 365.2+-0.1 m^2 are expected. Re-evaluation in Dec. 2017
% resulted in 365.4 m^2 projected. All on-axis.
% Evaluation with sim_telarray results in 380.37+-0.07 m^2 with the 300 cm square
% camera body and 377.71+-0.07 m^2 with the 348 cm square camera body assumption
% including the mounting frame.
telescope_transmission = 0.9606  % = 365.4/380.37 with 300 cm camera body accounted for explicitely.

% Equivalent circular diameter for older sim_telarray: 339 cm.
# ifdef BIGGER_CAMERA_SHADOW
camera_body_shape = 2        % Square camera body, needs newer sim_telarray
camera_body_diameter = 348   % cm (only for shadowing, includes mounting frame)
telescope_transmission = 0.9674 % = 365.4/377.71 with 348 cm camera body + frame accounted for explicitely.
# endif

camera_transmission = 1.0 % All of the transmission is now included in the camera_filter file.
camera_filter = transmission_lst_window_No7-10_ave.dat

% SiPM-based LST camera configuration
camera_config_file = camera_configuration.dat

#define ONLY_ANALOG_SUM 1
#define NO_MAJORITY 1
#define NO_DIGITAL_SUM 1

camera_pixels = 7420  % needs to be specified explicitly

store_photoelectrons = 2    % Save individual photo-electrons

% --------------------------- Trigger -----------------------------------

% The trigger simulation is over a slightly larger time window than FADC signals.
disc_bins = 50  % Number of time intervals simulated for trigger.
disc_start = 3  % How many intervals the trigger simulation starts before the ADC.

% The camera config file has majority, analog sum, plus digital sum.
#ifndef NO_ANALOG_SUM
# define ANALOG_SUM 1
#endif

% Majority & analog sum input pulses:

% Discriminator threshold (and corresponding multiplicity for telescope trigger):
discriminator_threshold = 99999

% Telescope trigger (specified even if no majority trigger is used):
default_trigger = AnalogSum
teltrig_min_time                       0.5 % ns
teltrig_min_sigsum                     7.8 % pV.s

trigger_delay_compensation = 0
trigger_current_limit = 2000.0

% At 1x/2x/5x/7x dark: 645/861/1234/1430 for "za" with clipping 350 (250 at 1x)
#ifdef THRESHOLD_5X
trigger_current_limit = 2000.0 % [microAmps]
nsb_scaling_factor = 5
asum_threshold = 1234   % "zg" at 5x dark with clipping 360
asum_clipping = 350 % mV
#endif
#ifdef THRESHOLD_7X
trigger_current_limit = 2000.0 % [microAmps]
nsb_scaling_factor = 5 % Yes simulation is actually at 5x dark NSB.
asum_threshold = 1430   % "zg" at 7x dark with clipping 360 (5x + safety margin)
asum_clipping = 350 % mV
#endif
#ifdef THRESHOLD_30X
trigger_current_limit = 2000.0 % [microAmps]
nsb_scaling_factor = 30
asum_threshold = 2000  % Just guessing; not meant to be usable at 30x dark (pixels may be disabled due to current)
asum_clipping = 350 % mV
min_photons = 100000000 % Bypass processing this telescope as early as possible
#endif

asum_shaping_file = none % No further shaping needed - pulse is wide enough.
asum_offset = 0.0

#ifdef LST_SAFE_TRIGGER
asum_threshold = 940 % mV, resulting in 8 kHz accidental single rate (NSB rate ~ CR single rate) at twice "dark" NSB.
asum_clipping = 450 % mV
% No high-NSB threshold values known
#endif
#ifdef LST_EXTREME_TRIGGER
asum_threshold = 645 % mV, extreme threshold: stereo (2/4 tel.) NSB random rate at 1x dark = 0.1 * CR stereo (2/4), single rate is about 40 kHz (unstable!!)
asum_clipping = 250 % mV
% No high-NSB threshold values known
#endif

only_triggered_telescopes=1

% ------------------------------ Readout --------------------------------

% Read-out of a 40 ns window (within simulated 55 ns) following the actual trigger:
fadc_bins = 55       % Number of time intervals simulated for ADC.
fadc_sum_bins = 40   % Number of ADC time intervals actually summed up or written as trace.
fadc_sum_offset = 9  % How many intervals summation starts before telescope trigger.

fadc_max_signal = 4095       % 12-bit ADC (applies to both channels unless specified)
fadc_max_sum = 16777215      % 24-bit sum is unlimited for all practical purposes.

% ----------------------------- Analysis --------------------------------

% Pulse shape analysis with pulse sum around global peak
% position only for significant pixels.
pulse_analysis = -30

% Pulse analysis provides a conditional 8 ns integration at 1000 MHz sampling rate.
sum_before_peak = 3
sum_after_peak = 4

tailcut_scale = 2.6 % For built-in image cleaning (not relevant for later analysis)

% ----------------------------------------------------------------------------
%
% Readout configuration : MUSIC with R=7, C=31.
%                         Electronics noise is 47.7% of gain
% Short waveform        : 7ns FWHM template is used
% Poor resolution       : We couldn't get good parameters for the SPE at 7ns.
%                         So we will use the SPE for 12 ns instead (ATTENTION)
% Sampling frequency    : 1 GHz (1000 MHz) Fast Sampling
%
% ----------------------------------------------------------------------------

% This file is meant to set or indicate useful DEFAULT values for all cameras with SiPM type photosensors.
% Value in the main configuration will override values from this file.
% Previously, a separated file (CTA-PROD4-sipm.cfg). Now included in the main configuration in this last part of the script.

% ------------------ Photomultipliers or SiPM sensors -------------------

% From previous LST small pixel simulation
quantum_efficiency = quantum_efficiency.dat

% Not every absorbed photon will result in an avalanche inside the silicon material. This loss is part of the photon detection efficiency.
pm_collection_efficiency = 1.0

% From previous LST small pixel simulation
pm_average_gain = 1e6

% SiPM's common value for SST sensors now
qe_variation = 0.03

% SiPM's random gain variation
gain_variation = 0.016

% That is 6 mm pixels with 50 micrometer cells (120x120).
pixel_cells = 14400

% Single Photo-Electron distribution (SPE response for all LST pixel cameras)
% LOOK CONFIG1 HEADER FOR WHY ->> 12ns instead of 7ns
pm_photoelectron_spectrum = spe_spectrum.dat

% SiPM change (From calculation using filter used above, SiPM pixels + LST window)
nightsky_background = all:0.386

% ------------------------------ Readout --------------------------------
% ------------------------------ DIGITAL --------------------------------
% ----------------------------- LSB units --------------------------------

% Sampling rate 1GSps (Sampling rate in MHz)
fadc_MHz = 1000

% Pulse shape (short, 07 ns long)
fadc_pulse_shape = pulse_shape.txt

% Total LSB for 12-bits                 : 2^12 = 4096 (range from 0 to 4095)
% Typical digital dynamic range         : 0 - 1800 mV
% Max. number of p.e. in dynamic range  : 1000 p.e.
% Convertion factor (LSB to p.e)        :
% CF = (Total LSB for 12-bits - 1) - fadc pedestal / Max. number of p.e. in dynamic range
% Convertion factor (p.e. to ph)        : cf = 1 p.e / 1.8 mV

fadc_sensitivity = 1        % FADC counts per mV voltage (or whatever unit is used forFADC_AMPlitude).
fadc_pedestal = 300         % Per time slice (positive signals only: unsigned!)
fadc_amplitude = 3.795      % CF, The peak amplitude in a time slice (discriminator_amplitude units, LSB)
fadc_noise = 1.81           % ratio * fadc_amplitude, ratio = sigma_e / gain (from MUSIC + SCOPE),

num_gains = 1               % Make it clear that we want to use 1 gain

asum_clipping = 632.5       % LSB

% ------------------------------ Trigger --------------------------------

% Majority & analog sum input pulses:

% 07 ns pulse width (FWHM) + 1GHz bandwidth
% Configuration : MUSIC with R=7, C=31
% Electronics noise is 47.7% of Gain (need to be introduce somewhere)
discriminator_pulse_shape = pulse_shape.txt

% SET THE UNITS FOR MANY OTHERS, LOOK IN DOC
% same value as fadc_amplitude and therefore in LSB
discriminator_amplitude = 3.795

% Telescope trigger (specified even if no majority trigger is used):
default_trigger = AnalogSum
teltrig_min_time                       0.5 % ns
teltrig_min_sigsum                     7.8 % pV.s

% ------------------------------ Yves parameters --------------------------------

pm_transit_time = 1.0      % Just a guess
transit_time_jitter  = 0.1 % Just a guess
pm_voltage_variation = 0.0 % Not a useful concept with SiPM
pm_gain_index = 0.0        % Irrelevant with zero voltage variation (gain as function of PMT high-voltage)
